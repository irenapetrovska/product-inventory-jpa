package com.persidacvetkoska.productinventoryjpa.inventory.product;

import com.persidacvetkoska.productinventoryjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.productinventoryjpa.inventory.Inventory;
import com.persidacvetkoska.productinventoryjpa.inventory.InventoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.text.html.Option;

import static org.junit.jupiter.api.Assertions.*;
import java.math.BigDecimal;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductService service;

    @Mock
    private ProductRepository productRepository;
    @Mock
    private InventoryRepository inventoryRepository;

    @Test
    public void given_create_product_request__when_create_product__then_create_product() {
        when(inventoryRepository.findById(any(Integer.class)))
                .thenReturn(Optional.of(new Inventory(1,"name", "location")));
        when(productRepository.save(any(Product.class)))
                .thenAnswer(invocationOnMock -> invocationOnMock.getArgument(0, Product.class));
        var request = new ProductCreateRequest( "name", "type", BigDecimal.valueOf(10), "description", 1);
        var expectedResult = new ProductDto(request.name, request.type, request.price, request.description, request.inventoryId);
        var result = service.create(request);
        assertNotNull(result);
        assertEquals(expectedResult, result);
    }

    @Test
    public void given_invalid_inventory_id__when_create_product__then_throw_exception() {
        when(inventoryRepository.findById(any()))
                .thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.create(new ProductCreateRequest()));
    }

    @Test
    public void given_valid_product_id__when_delete_product__then_delete_product(){
        var product = new Product();
        when(inventoryRepository.findProductById(any(), any()))
                .thenReturn(Optional.of(product));
        service.delete(0, 0);
        verify(productRepository, atLeastOnce())
                .delete(product);
    }

    @Test
    public void given_invalid_product_id__when_delete_product__then_throws_exception() {
        when(inventoryRepository.findProductById(any(), any()))
                .thenReturn(Optional.empty());
        assertThrows(ResourceNotFoundException.class, () -> service.delete(0,0));
    }
}
