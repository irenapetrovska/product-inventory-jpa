package com.persidacvetkoska.productinventoryjpa.config;


import com.persidacvetkoska.productinventoryjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.productinventoryjpa.exception.ResourceValidationException;
import com.persidacvetkoska.productinventoryjpa.utils.ErrorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;


@ControllerAdvice
@Component
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class.getName());

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handle(ResourceNotFoundException exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDto(exception.getMessage());
    }
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handle(ResourceValidationException exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDto(exception.getMessage());
    }
    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handle(ConstraintViolationException exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDto(exception.getMessage());
    }

    @ExceptionHandler
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handle(Exception exception){
        LOGGER.error(exception.getMessage(), exception);
        return new ErrorDto("An error has occurred! Please contact your administrator!");
    }
}
