package com.persidacvetkoska.productinventoryjpa.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("ssh")
public class SecurityProperties {

    private Resource publicKey;
}