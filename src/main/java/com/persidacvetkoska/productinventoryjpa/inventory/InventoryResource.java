package com.persidacvetkoska.productinventoryjpa.inventory;

import com.persidacvetkoska.productinventoryjpa.inventory.product.ProductDto;
import com.persidacvetkoska.productinventoryjpa.utils.VoidResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventories")
public class InventoryResource {

    private InventoryService inventoryService;

    public InventoryResource(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDto create (@RequestBody final InventoryRequest request){
        return inventoryService.create(request);
    }

       @PreAuthorize("hasAnyAuthority({'AUTHORIZED_ADMIN'})")
    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<InventoryDto> findPage(
            @RequestParam(value = "location", required = false) String location, @RequestParam(value = "name", required = false) String name, Pageable pageable){
        return inventoryService.findPage(new InventorySearchRequest(location, name, pageable));
    }

    @GetMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDto findInventoryById(@PathVariable(value = "id") Integer id){
        return inventoryService.findInventoryById(id);
    }

    @GetMapping(path = "/find-by-name/{name}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDto findByName(@PathVariable("name") String name){
        return inventoryService.findByNameNative(name);
    }

    @GetMapping(path = "/find-products-by-type", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
        public List<ProductDto> findAllProductsByType(@RequestParam String type, @RequestParam int inventoryId){
        return inventoryService.findAllProductsByType(type, inventoryId);
    }

    @GetMapping(path = "/find-products-by-name", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ProductDto> findProductsByName(@RequestParam String name, @RequestParam Integer inventoryId){
        return inventoryService.findProductsByName(name, inventoryId);
    }

    @GetMapping(path = "/print-all-products", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public List<ProductDto> returnAllProducts(@RequestParam Integer inventoryId){
        return inventoryService.returnAllProducts(inventoryId);
    }

    @PutMapping(path="/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public InventoryDto update(@PathVariable Integer id, @RequestBody InventoryRequest request){
        return inventoryService.update(id, request);
    }

    @DeleteMapping(path="/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public VoidResponse delete(@PathVariable Integer id){
        inventoryService.delete(id);
        return new VoidResponse(true);
    }
}
