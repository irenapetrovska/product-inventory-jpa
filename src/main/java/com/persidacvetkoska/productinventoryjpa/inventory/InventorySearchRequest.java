package com.persidacvetkoska.productinventoryjpa.inventory;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class InventorySearchRequest {

    public String name;
    public String location;
    public Pageable pageable;

    public InventorySearchRequest(String name, String location, Pageable pageable) {
        this.name = name;
        this.location = location;
        this.pageable = pageable;
    }

    public Specification<Inventory> generateSpecification(){
        Specification<Inventory> specifications = Specification.where(null);

        if (StringUtils.hasText(name)){
            specifications.and(InventorySpecifications.byNameLiteralEquals(name));
        }
        if (StringUtils.hasText(location)){
            specifications.and(InventorySpecifications.byLocationLiteralEquals(location));
        }

        return specifications;
    }

}
