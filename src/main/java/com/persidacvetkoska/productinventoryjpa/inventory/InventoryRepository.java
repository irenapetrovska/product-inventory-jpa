package com.persidacvetkoska.productinventoryjpa.inventory;

import com.persidacvetkoska.productinventoryjpa.inventory.product.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface InventoryRepository extends JpaRepository<Inventory, Integer> , JpaSpecificationExecutor<Inventory> {

        @Query("select product from Product product inner join product.inventory inventory where product.type = :type and inventory.id = :inventoryId")
        List<Product> findAllProductsByType(String type, int inventoryId);

        @Query("select product from Product product inner join product.inventory inventory where product.id = :productId and inventory.id = :inventoryId")
        Optional<Product> findProductById(Integer productId, Integer inventoryId);

        @Query("select product from Product product inner join product.inventory inventory where lower(product.name) like '%'||lower(:productName)||'%' and inventory.id = :inventoryId")
        List<Product> findProductsByName(String productName, Integer inventoryId);

        @Query("select product from Product product inner join product.inventory inventory where inventory.id = :inventoryId")
        List<Product> returnAllProducts(Integer inventoryId);

        Optional<Inventory> findFirstByName(String name);

        @Query(nativeQuery = true, value = "select * from inventory where name = :name limit 1")
        Optional<Inventory> findFirstByNameNative(String name);

        @Query(nativeQuery = true, value = "select * from inventory where name = coalesce(CAST(:name as VARCHAR), name) " +
                " and location = coalesce(CAST(:location as VARCHAR), location)",
                countQuery = "select count(*) from inventory where name = coalesce(CAST(:name as VARCHAR), name) " +
                " and location = coalesce(CAST(:location as VARCHAR), location)")
        Page<Inventory> findPage(final String name, final String location, Pageable pageable);


}
