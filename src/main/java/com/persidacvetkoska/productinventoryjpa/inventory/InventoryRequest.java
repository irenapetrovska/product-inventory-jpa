package com.persidacvetkoska.productinventoryjpa.inventory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class InventoryRequest {

    public String name;
    public String location;


}
