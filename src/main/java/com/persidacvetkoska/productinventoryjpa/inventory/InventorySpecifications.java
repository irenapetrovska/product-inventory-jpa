package com.persidacvetkoska.productinventoryjpa.inventory;

import org.springframework.data.jpa.domain.Specification;


public final class InventorySpecifications {

    private InventorySpecifications(){}

    public static Specification<Inventory> byNameLiteralEquals(final String name) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("name")), name.toLowerCase());
    }

    public static Specification<Inventory> byLocationLiteralEquals(final String location) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(criteriaBuilder.lower(root.get("location")), location.toLowerCase());
    }
}
