package com.persidacvetkoska.productinventoryjpa.inventory.product;

import com.persidacvetkoska.productinventoryjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.productinventoryjpa.inventory.InventoryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.transaction.Transactional;
import javax.validation.Valid;

@Service
@Validated
public class ProductService {

    private ProductRepository productRepository;
    private InventoryRepository inventoryRepository;

    public ProductService(ProductRepository productRepository, InventoryRepository inventoryRepository) {
        this.productRepository = productRepository;
        this.inventoryRepository = inventoryRepository;
    }

    public Page<ProductDto> getPage(Integer inventoryId, Pageable pageable){
        return this.productRepository.findByInventoryId(inventoryId, pageable).map(product -> product.toDto());
    }

    public Page<ProductDto> findAllPage(Pageable pageable){
        return this.productRepository.findAll(pageable).map(product -> product.toDto());
    }

    @Transactional
    public ProductDto create(@Valid final ProductCreateRequest request){
        var inventory = inventoryRepository.findById(request.inventoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Inventory with id = " + request.inventoryId + "doesn't exist!"));
        var product = new Product(request.name, request.type, request.price, request.description, inventory);
        var createdProduct = productRepository.save(product);
        return createdProduct.toDto();
    }

    public ProductDto findProductById(Integer productId){
        Product foundProduct = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product with id= " + productId + " not found"));
        return foundProduct.toDto();
    }

    @Transactional
    public ProductDto delete(final Integer productId, final Integer inventoryId){
       Product foundProduct = inventoryRepository.findProductById(productId, inventoryId)
               .orElseThrow(() -> new ResourceNotFoundException("Product with id= " + productId + " not found in inventory with id= " + inventoryId));
       productRepository.delete(foundProduct);
       return foundProduct.toDto();
    }

    @Transactional
    public ProductDto update(@Valid ProductRequest request){
        Product foundProduct = inventoryRepository.findProductById(request.id, request.inventoryId).orElseThrow(() -> new ResourceNotFoundException("Product not found!"));
        var productForUpdate = new Product(request.id, request.name, request.type, request.price, request.description, foundProduct.inventory);
        var updatedProduct = productRepository.save(productForUpdate);
        return updatedProduct.toDto();
    }
}
