package com.persidacvetkoska.productinventoryjpa.inventory.product;


import com.persidacvetkoska.productinventoryjpa.inventory.Inventory;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name="product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;
    @Column public String name;
    @Column public String type;
    @Column public BigDecimal price;
    @Column public String description;
    @ManyToOne
    @JoinColumn(name = "inventory_id")
    public Inventory inventory;

    public Product( String name, String type, BigDecimal price, String description, Inventory inventory) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
        this.inventory = inventory;
    }

    public Product( Integer id, String name, String type, BigDecimal price, String description, Inventory inventory) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.description = description;
        this.inventory = inventory;
    }


    public Product() {
    }

    public ProductDto toDto(){
        return new ProductDto(id, name, type, price, description, inventory.getId());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) && Objects.equals(name, product.name) && Objects.equals(type, product.type) && Objects.equals(price, product.price) && Objects.equals(description, product.description) && Objects.equals(inventory, product.inventory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }
}
