package com.persidacvetkoska.productinventoryjpa.inventory.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    public Page<Product> findByInventoryId(Integer inventoryId, Pageable pageable);

}
