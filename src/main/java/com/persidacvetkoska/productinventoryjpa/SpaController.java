package com.persidacvetkoska.productinventoryjpa;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

@AllArgsConstructor
@Controller
public class SpaController {

    @GetMapping({"/{path:[^\\.]*}", "/**/{path:^(?!oauth).*}/{path:[^\\.]*}"})
    public String forward(HttpServletRequest request) {
        return "forward:/index.html";
    }
}
