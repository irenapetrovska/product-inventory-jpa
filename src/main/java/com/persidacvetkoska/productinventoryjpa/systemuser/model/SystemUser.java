package com.persidacvetkoska.productinventoryjpa.systemuser.model;

import com.persidacvetkoska.productinventoryjpa.systemuser.dto.SystemUserDTO;
import lombok.ToString;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
@ToString
public class SystemUser {

    @Id
    public String username;
    public String password;
    @Column(name = "full_name")
    public String fullName;
    public String email;
    public String address;

    public boolean enabled;
    @Column(name = "created_at")
    public Instant createdAt;

    @Column(name = "activation_key")
    public String activationKey;

    @Column(name = "activation_key_created_at")
    public Instant activationKeyCreatedAt;

    @Column(name = "reset_key")
    public String resetKey;

    @Column(name = "reset_key_created_at")
    public Instant resetKeyCreatedAt;

    public SystemUser(String username, String fullName, String email, String address, Instant createdAt,boolean enabled) {
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.address = address;
        this.createdAt = createdAt;
        this.enabled = enabled;
    }

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    }, fetch = FetchType.EAGER)
    @JoinTable(name = "group_members",
            joinColumns = @JoinColumn(name = "username"),
            inverseJoinColumns = @JoinColumn(name = "group_id")
    )
    public Set<Group> groups = new HashSet<>();

    public SystemUser() {
    }

    public Group addGroup(final Group group) {
        groups.add(group);
        group.users.add(this);
        return group;
    }

    public Group removeGroup(final Group group) {
        groups.remove(group);
        group.users.remove(this);
        return group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemUser user = (SystemUser) o;
        return Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(31);
    }

    public SystemUserDTO toDTO(){
        return new SystemUserDTO(fullName, username, email, address);
    }
    
}
