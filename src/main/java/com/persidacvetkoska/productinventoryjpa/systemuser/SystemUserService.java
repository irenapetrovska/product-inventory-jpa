package com.persidacvetkoska.productinventoryjpa.systemuser;


import com.persidacvetkoska.productinventoryjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.productinventoryjpa.systemuser.dto.SystemUserDTO;
import com.persidacvetkoska.productinventoryjpa.systemuser.dto.SystemUserRegisterRequest;
import com.persidacvetkoska.productinventoryjpa.systemuser.model.SystemUser;
import com.persidacvetkoska.productinventoryjpa.systemuser.repository.GroupRepository;
import com.persidacvetkoska.productinventoryjpa.systemuser.repository.SystemUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import javax.validation.Valid;

@Service
@RequiredArgsConstructor
public class SystemUserService {

    private final SystemUserRepository systemUserRepository;
    private final PasswordEncoder encoder;
    private final GroupRepository groupRepository;

    @Transactional
    public SystemUserDTO register(@Valid final SystemUserRegisterRequest request){
        var user = request.toEntity();
        var group = groupRepository.findGroupByGroupName("USER_GROUP")
                .orElseThrow(() -> new ResourceNotFoundException("User group is not found"));
        user.addGroup(group);
        user.password = encoder.encode(request.password);
        return systemUserRepository.save(user).toDTO();
    }

    public Page<SystemUserDTO> fetchPage(final Pageable pageable) {
        return systemUserRepository.findAll(pageable)
                .map(SystemUser::toDTO);
    }

    public SystemUserDTO fetchByUsername(final String username) {
        return systemUserRepository.findById(username)
                .map(SystemUser::toDTO)
                .orElseThrow(() -> new ResourceNotFoundException("User not found!!"));
    }
//
//
//
//    public UserView edit(final EditPlayerDetailsRequest request, final String username) {
//        final var city = findCityById(request.cityId);
//        final var systemUser = findUserById(username).update(request, city);
//        return systemUserRepository.save(systemUser).toView();
//    }

//

    //
//    public SystemUserDTO addClient(
//            @Valid @NotNull(message = "{user.add-player-request-missing}")
//            final AddClientRequest request) {
//        log.debug("Add client with request [{}]", request);
//
//        final var existingUserById = systemUserRepository.findById(request.userName);
//        if (existingUserById.isPresent()) {
//            throw new ResourceValidationException("User already registered!!");
//        }
//
//        final var existingUserByEmail = systemUserRepository.findByEmail(request.email);
//        if (existingUserByEmail.isPresent()) {
//            throw new ResourceValidationException("User already registered!!");
//        }
//
//        var user = new SystemUser();
//        user.username = request.userName;
//        user.email = request.email;
//        user.password = passwordEncoder.encode(request.password);
//        user.enabled = false;
//        user.firstName = request.firstName;
//        user.lastName = request.lastName;
//        user.address = request.address;
//        user.dateOfBirth = request.dateOfBirth;
//        user.city = request.city;
//        user.country = request.country;
//        user.createdAt = Instant.now();
//        user.role = Role.CLIENT;
//        user.enabled = true;
//
//        Group group = groupRepository.findByGroupName("CLIENT_ONLY_GROUP");
//        user.addGroup(group);
//
//        systemUserRepository.save(user);
//
//        log.debug("Register player with request [{}] completed!", request);
//        return user.toDTO();
//    }
//
//    public void deletePlayer(final String username) {
//        final var systemUser = findUserById(username);
//        if (systemUserRepository.playerHasRelations(username) > 0) {
//            throw new ResourceValidationException(MessageCode.USER_HAS_VOUCHERS_CANT_DELETE, MessageCode.USER_HAS_VOUCHERS_CANT_DELETE.getCode());
//        }
//        systemUserRepository.delete(systemUser);
//    }
//
//    private EmailRequest generateUserActivatedEmailRequest(final SystemUser systemUser) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                systemUser.email,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_ACCOUNT_ACTIVATED_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser)
//        );
//    }
//
//    private EmailRequest generatePasswordResetEmailRequest(final SystemUser systemUser) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                systemUser.email,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_ACCOUNT_RESET_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser)
//        );
//    }
//
//    private EmailRequest generateActivationEmailRequest(final SystemUser systemUser) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                systemUser.email,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_ACCOUNT_ACTIVATION_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser)
//        );
//    }
//
//    private EmailRequest generateReferralEmailRequest(final SystemUser systemUser, final String referredToEmail) {
//        return new EmailRequest(
//                EMAIL_FROM,
//                referredToEmail,
//                messageTranslator.getTextForKey(MessageCode.EMAIL_REFERRAL_TITLE.getCode()),
//                null,
//                Map.of(EMAIL_MODEL, systemUser, "referralId", systemUser.username, "referredTo", referredToEmail)
//        );
//    }
//
//    private SystemUser findUserById(final String id) {
//        return systemUserRepository.findById(id)
//                .orElseThrow(() -> new ResourceNotFoundException(
//                        MessageCode.USER_NOT_FOUND, MessageCode.USER_NOT_FOUND.getCode()));
//    }
//
//    private SystemUser findUserByEmail(final String email) {
//        return systemUserRepository.findByEmail(email)
//                .orElseThrow(() -> new ResourceNotFoundException(
//                        MessageCode.USER_NOT_FOUND, MessageCode.USER_NOT_FOUND.getCode()));
//    }
//
//    private City findCityById(final long id) {
//        return cityRepository.findById(id)
//                .orElseThrow(() -> new ResourceValidationException(
//                        MessageCode.USER_REGISTER_CITY_REQUIRED, MessageCode.USER_REGISTER_CITY_REQUIRED.getCode()));
//    }
}
