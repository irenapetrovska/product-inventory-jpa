package com.persidacvetkoska.productinventoryjpa.systemuser.repository;

import com.persidacvetkoska.productinventoryjpa.systemuser.model.Group;
import com.persidacvetkoska.productinventoryjpa.systemuser.model.SystemUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface SystemUserRepository extends JpaRepository<SystemUser, String> {

    Optional<SystemUser> findByEmail(final String email);

    Page<SystemUser> findAll(Pageable pageable);
}
