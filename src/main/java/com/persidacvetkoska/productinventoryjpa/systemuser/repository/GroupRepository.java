package com.persidacvetkoska.productinventoryjpa.systemuser.repository;

import com.persidacvetkoska.productinventoryjpa.systemuser.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface GroupRepository extends JpaRepository<Group, Integer> {

    Optional<Group> findGroupByGroupName(String name);
}
