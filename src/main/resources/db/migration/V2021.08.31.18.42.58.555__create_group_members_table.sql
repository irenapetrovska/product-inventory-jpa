CREATE TABLE public.group_members
 (
     id integer NOT NULL ,
     username character varying(255)  NOT NULL,
     group_id integer NOT NULL,
     created_at timestamp with time zone,
     modified_at timestamp with time zone,
     CONSTRAINT group_members_pkey PRIMARY KEY (id),
     CONSTRAINT fk_group_members_group FOREIGN KEY (group_id)
         REFERENCES public.groups (id) MATCH SIMPLE
         ON UPDATE NO ACTION
         ON DELETE NO ACTION,
     CONSTRAINT fk_group_members_users FOREIGN KEY (username)
         REFERENCES public.users (username) MATCH SIMPLE
         ON UPDATE NO ACTION
         ON DELETE NO ACTION
 )