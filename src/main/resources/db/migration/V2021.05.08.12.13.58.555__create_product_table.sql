CREATE TABLE public.product
(
    id integer NOT NULL,
    type character varying(50) NOT NULL,
    price numeric(8,2) NOT NULL,
    description character varying(1000) ,
    inventory_id integer,
    name character varying(50) ,
    CONSTRAINT product_pkey PRIMARY KEY (id),
    CONSTRAINT fk_product_inventory_id FOREIGN KEY (inventory_id)
        REFERENCES public.inventory (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE)