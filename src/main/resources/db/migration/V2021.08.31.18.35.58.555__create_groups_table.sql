CREATE TABLE public.groups
(
    id integer NOT NULL ,
    group_name character varying(50)  NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT groups_pkey PRIMARY KEY (id)
)