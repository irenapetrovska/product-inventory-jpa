
CREATE TABLE public.oauth_client_details
(
    client_id character varying(256)  NOT NULL,
    resource_ids character varying(256) ,
    client_secret character varying(256) NOT NULL,
    scope character varying(256) ,
    authorized_grant_types character varying(256) ,
    web_server_redirect_uri character varying(256) ,
    authorities character varying(256) ,
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information character varying(4000) ,
    autoapprove character varying(256) ,
    CONSTRAINT oauth_client_details_pkey PRIMARY KEY (client_id)
)