CREATE TABLE public.oauth_refresh_token
(
    token_id character varying(256) NOT NULL,
    token bytea,
    authentication bytea,
    CONSTRAINT oauth_refresh_token_pkey PRIMARY KEY (token_id)
)