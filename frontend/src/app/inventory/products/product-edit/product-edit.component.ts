import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  public productId: number|undefined;
  public productForm = new FormGroup({
    name: new FormControl(null, Validators.maxLength(50)),
    type: new FormControl(null, Validators.required),
    price: new FormControl(null, Validators.required),
    description: new FormControl(null, Validators.maxLength(1000))
  })

  constructor(private route: ActivatedRoute, private productService: ProductService) { }

  ngOnInit(): void {

    const rawProductId = this.route.snapshot.paramMap.get('productId');

    if(!rawProductId){
      console.log('Product Id not provided!');
      return;
    }
    this.productId = Number(rawProductId);
    this.productService.getProductById(this.productId).subscribe({
      next: response => {
        this.productForm.get('name')?.setValue(response.name);
        this.productForm.get('type')?.setValue(response.type);
        this.productForm.get('price')?.setValue(response.price);
        this.productForm.get('description')?.setValue(response.description);
      }
    })

  }

  onSubmit(): void {}

}
