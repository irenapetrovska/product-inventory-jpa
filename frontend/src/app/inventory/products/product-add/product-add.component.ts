import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../product.service';
import { ProductRequest } from '../products.domain';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {

  public productForm = new FormGroup({
    name: new FormControl(null, Validators.maxLength(50)),
    type: new FormControl(null, Validators.required),
    price: new FormControl(null, Validators.required),
    description: new FormControl(null, Validators.maxLength(1000))
  });

  constructor(private productService: ProductService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
  }

  onSubmit():void {
    if(this.productForm.invalid){
      console.log('This form is invalid!');
      return;
    }
    const name = this.productForm.value.name;
    const type = this.productForm.value.type;
    const price = this.productForm.value.price;
    const description = this.productForm.value.description;

    const rawInventoryId = this.route.snapshot.paramMap.get('id');

    if (!rawInventoryId) {
      console.log('Inventory ID is required!!!');
      return;
    }
    const inventoryId = Number(rawInventoryId);
    this.productService.create(new ProductRequest(name, type, price, description, inventoryId)).subscribe({
      next: _ => this.router.navigate([`../`], {relativeTo: this.route})
    })
  }

}
