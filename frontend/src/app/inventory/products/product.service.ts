import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Product, ProductRequest } from './products.domain';
import { Page } from '../../shared/shared.domain';
import { environment } from 'src/environments/environment';

const API_ENDPOINT = 'products';
const HEADERS = new HttpHeaders({
  Accept: 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  public page:number = 0;
  public size:number = 0;

  public getPage(page: number, size: number, inventoryId:number): Observable<Page<Product>> {
    const params = new HttpParams()
    .append('page', page)
    .append('size', size)
    .append('inventoryId', inventoryId);
    
    return this.httpClient.get<Page<Product>>(`${environment.apiUrl}/${API_ENDPOINT}`, {headers: HEADERS, params: params}).pipe(
      catchError((error) => {
        console.error(error);
        return of(new Page<Product>([], 0, 0));
      })
    );
}

public getAllPage(page: number, size: number): Observable<Page<Product>> {
  const params = new HttpParams()
  .append('page', page)
  .append('size', size);
  
  return this.httpClient.get<Page<Product>>(`${environment.apiUrl}/${API_ENDPOINT}/all`, {headers: HEADERS, params: params}).pipe(
    catchError((error) => {
      console.error(error);
      return of(new Page<Product>([], 0, 0));
    })
  );
}

public getProductById(productId: number): Observable<Product>{
  return this.httpClient.get<Product>(`${environment.apiUrl}/${API_ENDPOINT}/${productId}`, {headers: HEADERS}).pipe(
    catchError(error => {
      console.error(error);
      return throwError(error);
    })
  );
}

public create(request: ProductRequest): Observable<Product> {
  return this.httpClient.post<Product>(`${environment.apiUrl}/${API_ENDPOINT}`, request, {headers: HEADERS}).pipe(
    catchError(error => {
      console.error(error);
      return throwError(error);
    })
  )
}

public delete(inventoryId:number, productId:number): Observable<Product> {
  const params = new HttpParams()
  .append('inventoryId', inventoryId)
  .append('productId', productId)
  return this.httpClient.delete<any>(`${environment.apiUrl}/${API_ENDPOINT}`, {headers: HEADERS, params:params}).pipe(
    catchError(error => {
      console.error(error);
      return throwError(error);
    })
  )
}

  constructor(private httpClient: HttpClient) { }
}
