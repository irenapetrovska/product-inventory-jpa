import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmationPopupComponent } from 'src/app/shared/confirmation-popup/confirmation-popup.component';
import { Page } from 'src/app/shared/shared.domain';
import { ProductService } from '../product.service';
import { Product } from '../products.domain';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.scss']
})
export class AllProductsComponent implements OnInit {

  public page: Page<Product> = new Page<Product>([], 0, 0);

  constructor(private productService: ProductService, private router: Router, 
    private bsModalRef: BsModalRef, private modalService: BsModalService ) { }

  ngOnInit(): void {
    this.productService.getAllPage(0, 10).subscribe({
      next: response => this.page = response
    })
  }

  public onEdit(inventoryId: number, productId:number): void {
    this.router.navigateByUrl(`inventories/${inventoryId}/products/${productId}`);
  }

  public onDelete(inventoryId:number, productId: number): void {
   

    const initialState = {
      modalText: `Are you sure you want to delete the product with id: ${productId}`
    }

    this.bsModalRef = this.modalService.show(ConfirmationPopupComponent, {initialState});
    this.bsModalRef.onHide?.subscribe(_ =>{
      if(this.bsModalRef?.content.confirmed){
        this.productService.delete(inventoryId,productId).subscribe({
          next: _ => this.productService.getAllPage(0,10)
        }
        );
      }
      this.productService.getAllPage(0,10);
    });

  }

}
