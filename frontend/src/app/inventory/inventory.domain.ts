export class Inventory{
    constructor(
        public id: number,
        public name: string,
        public location:string
    ){}
}
export class InventoryRequest{
    constructor(
        public name: string,
        public location:string
    ){}
}