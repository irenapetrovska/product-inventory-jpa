import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthorizationService } from '../shared/services/authorization.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public loginForm = new FormGroup({
    username: new FormControl(null, []),
    password: new FormControl(null, [Validators.minLength(8)])
  })

  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit(): void {
  }

  public onSubmit(){
    if(this.loginForm.invalid) {
      console.log('Form is not valid!');
      console.log(this.loginForm.errors);
      return;
    }
      this.authorizationService.login(this.loginForm.value.username, this.loginForm.value.password);
  }

}
