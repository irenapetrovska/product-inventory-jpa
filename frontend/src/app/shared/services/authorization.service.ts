import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AlertService } from '../components/alert/alert.service';

const API_AUTH='oauth/token';
const encodedKey = btoa('public-client:some-secret');
const headers = new HttpHeaders({
    'Content-type': 'application/x-www-form-urlencoded',
    'Authorization': `Basic ${encodedKey}`,
    'Accept': 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private httpClient: HttpClient, private router: Router, private alertService: AlertService) { }

  public login(username: string, password: string): void {
    const requestParams = new HttpParams()
    .append('username', username)
    .append('password', password)
    .append('grant_type', 'password')

    this.httpClient.post(`${environment.domainUrl}/${API_AUTH}`, requestParams, {headers:headers}).subscribe({
      next: response => {
        localStorage.setItem('authorization', JSON.stringify(response));
        this.router.navigateByUrl('inventories');
    },
    error: error => this.alertService.errorAlert(error)
  });

  }

  public refreshToken(): Observable<any> {
    const requestParams = new HttpParams()
    .append('refresh_token', this.getRefreshToken() as string)
    .append('grant_type', 'refresh_token')

    return this.httpClient.post(`${environment.domainUrl}/${API_AUTH}`, requestParams, {headers:headers}).pipe(
      tap(response => localStorage.setItem('authorization', JSON.stringify(response))),
      catchError(error => {
        console.error(error);
        this.logOut;
        return throwError(error);
      })
    )
    
    // subscribe({
    //   next: response => {
    //     localStorage.setItem('authorization', JSON.stringify(response));
    // },
    // error: error => {
    //   console.error(error);
    //   this.logOut();
    // }
  

  }

  public isLoggedIn(): boolean{
    const authorization = localStorage.getItem('authorization');
    return !!authorization;
  }

  public logOut(): void {
    localStorage.removeItem('authorization');
    this.router.navigateByUrl('login');
  }

  public getAccessToken():string | null{
    
    return this.getAuthorizationFromLocalStorage('authorization').access_token;
  }

  public getRefreshToken():string | null{
  
    return this.getAuthorizationFromLocalStorage('authorization').refresh_token;
  }

  private getAuthorizationFromLocalStorage(key: string): any|null {
    const authorizationString = localStorage.getItem(key);
    if (!authorizationString){
      return null;
    }
    const jsonObject = JSON.parse(authorizationString);
    return jsonObject;

  }
}
