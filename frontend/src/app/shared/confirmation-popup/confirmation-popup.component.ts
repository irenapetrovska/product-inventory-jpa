import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirmation-popup',
  templateUrl: './confirmation-popup.component.html',
  styleUrls: ['./confirmation-popup.component.scss']
})
export class ConfirmationPopupComponent implements OnInit {
  public confirmed: boolean = false;


  modalText: string = 'Are you sure that you want to proceed with the action?';
 
  constructor(public bsModalRef: BsModalRef) {}
 
  ngOnInit() {
  }

  onYes() {
    this.confirmed = true;
    this.bsModalRef.hide();
  }

  onNo() {
    this.confirmed = false;
    this.bsModalRef.hide();
  }


}
