import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthorizationService } from '../services';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  constructor(private authorizationService: AuthorizationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(this.authorizationService.isLoggedIn()){
      request = this.updateRequest(request, this.authorizationService.getAccessToken())
    }
    return next.handle(request).pipe(
      catchError(error => {
        if(error instanceof HttpErrorResponse && error.status === 401 && !request.params.get('refresh_token')){
             this.handle401WithReload(request, next);
        }
        return throwError(error);
      })
    );
  }

  private handle401(request: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<unknown>>{
    return this.authorizationService.refreshToken().pipe(
      switchMap((authentication: any) => {
        const accessToken = authentication.access_token;
        request = this.updateRequest(request, accessToken);
        return next.handle(request);
      })
  );
  }

  private handle401WithReload(request: HttpRequest<any>, next: HttpHandler):void{
     this.authorizationService.refreshToken().pipe(
      // switchMap((authentication: any) => {
      //   const accessToken = authentication.access_token;
      //   request = this.updateRequest(request, accessToken);
      //   return next.handle(request);
      // })
      tap((authentication : any) => {
        location.reload();
      })
  ).subscribe(); 
  }

  private updateRequest(request: HttpRequest<any>, token: string|null):HttpRequest<any> {
    // const currentLang = localStorage.getItem(AppConsts.langKey);
    if (request.url.indexOf('oauth') < 0 && token) { 
        request = request.clone({headers: request.headers.set('Authorization', `Bearer ${token}`)});
    }
    if (!request.headers.has('Content-Type') && request.url.indexOf('upload') < 0) {
        request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
    }
    // if (!request.params.has('lang') && request.url.indexOf('oauth') < 0) {
    //     request = request.clone({params: request.params.set('lang', 'mk')});
    // }

    return request;
}
}
