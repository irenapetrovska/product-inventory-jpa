import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryListComponent } from './inventory/inventory-list/inventory-list.component';
import { InventoryComponent } from './inventory/inventory.component';
import { AllProductsComponent } from './inventory/products/all-products/all-products.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  {path: 'inventories', loadChildren: () => import('./inventory/inventory.module').then(m => m.InventoryModule),
canActivate: [AuthGuard]
},
{ path: 'login', component: LoginComponent},
// { path: 'register', component: RegisterComponent},
{ path: '', redirectTo: 'login', pathMatch: 'full'},
{ path: '**', redirectTo: 'inventories', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
