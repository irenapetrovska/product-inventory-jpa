import { Component, OnInit } from '@angular/core';
import { Alert } from './shared/components/alert/alert.domain';
import { AlertService } from './shared/components/alert/alert.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'frontend';

  constructor(private alertService:AlertService){

  }

 ngOnInit(){
  //  this.alertService.successAlert('Sucess message!');
 }

}
